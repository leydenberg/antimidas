from mpl_events import MplEventDispatcher, mpl
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.colors import LogNorm


def intenAlongLine(data, xCen, yCen, ang=90, measureCoeff=1):
    ySize, xSize = data.shape
    radAng = ang*np.pi/180
    if abs(np.tan(radAng)) > ySize/xSize:
        zSize = abs(ySize/np.sin(radAng)*measureCoeff) - 1
        zCen = yCen/abs(np.sin(radAng))*measureCoeff - 1
    else:
        zSize = abs(xSize/np.cos(radAng))*measureCoeff - 1
        zCen = xCen/np.cos(radAng)*measureCoeff - 1
    cosAng = np.cos(radAng)
    sinAng = np.sin(radAng)
    inten = np.zeros(int(zSize))
    xShift = xCen-zCen/measureCoeff*cosAng
    yShift = yCen-zCen/measureCoeff*sinAng
    for z in range(int(zSize)):
        floatX, intX = np.modf(z/measureCoeff*cosAng+xShift)
        floatY, intY = np.modf(z/measureCoeff*sinAng+yShift)
        intX = int(intX)
        intY = int(intY)
        if (intX > 0) and (intX < xSize-1) and (intY > 0) and (intY < ySize-1):
            inten[z] = ((1.0-floatY)*(1.0-floatX) * data[intY][intX] +
                        floatY*(1.0-floatX) * data[intY+1][intX] +
                        floatX*(1.0-floatY) * data[intY][intX+1] +
                        floatY*floatX * data[intY+1][intX+1])
    return inten


class SelectCenterAndAngDispatcher(MplEventDispatcher):
    def __init__(self, figure, data, regionSize=25):
        super().__init__(figure)
        self.fig = figure
        self.center = (0, 0)
        self.ang = 0
        self.centerSelected = False
        self.data = data
        self.regionSize = regionSize

    def on_mouse_button_release(self, event: mpl.MouseEvent):
        if not self.centerSelected:
            self.center = (event.xdata, event.ydata)
            rect = patches.Rectangle((event.xdata-self.regionSize, event.ydata-self.regionSize),
                                     2*self.regionSize, 2*self.regionSize, linewidth=0.5,
                                     edgecolor='r', facecolor='none')
            self.fig.axes[0].add_patch(rect)
            self.center = findPick(self.data, self.center, self.regionSize)
            self.fig.axes[0].plot(self.center[0], self.center[1], 'r,')
            self.fig.canvas.draw_idle()
            self.centerSelected = True
        else:
            self.ang = np.arctan(
                (event.xdata - self.center[0])/(event.ydata - self.center[1]))
            self.ang *= 180/np.pi
            plt.close('all')


def findPick(data, center, regionSize):
    xCen, yCen = map(lambda x: int(round(x)), center)
    centerRegion = data[yCen-regionSize:yCen +
                        regionSize, xCen-regionSize:xCen+regionSize]
    yPick, xPick = np.unravel_index(
        np.argmin(centerRegion, axis=None), centerRegion.shape)
    return (xPick + (xCen-regionSize), yPick + (yCen-regionSize))


def selectCenterAndAng(data, v=(22, 16.5)):
    fig, ax = plt.subplots()
    cenDisp = SelectCenterAndAngDispatcher(fig, data)
    vmin, vmax = v
    im = ax.imshow(data, cmap=plt.cm.bone, origin='lower',
                   norm=LogNorm(vmin=vmin, vmax=vmax),)
    ax.set_title("Select Center And Ang")
    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel('value', rotation=-90, va="bottom")
    ax.contour(data, levels=np.linspace(
        v[1], v[0], 20), colors='black', alpha=0.5)
    plt.show()
    return cenDisp.center[1], cenDisp.center[0], cenDisp.ang


def getSlice(data, xCen, yCen, ang=90, galBoxSize=None, v=(22.5, 16), measureCoeff=1):
    if galBoxSize:
        galBoxX1 = xCen - galBoxSize
        galBoxY1 = yCen - galBoxSize
        galBoxX2 = xCen + galBoxSize
        galBoxY2 = yCen + galBoxSize
        boxedData = data[galBoxX1:galBoxX2, galBoxY1:galBoxY2]
        xCen = yCen = galBoxSize
    else:
        boxedData = data
    intenMajorAx = intenAlongLine(boxedData, xCen, yCen, ang=ang,
                                  measureCoeff=measureCoeff)
    intenMinorAx = intenAlongLine(boxedData, xCen, yCen, ang=ang-90,
                                  measureCoeff=measureCoeff)

    MaAxLen = len(intenMajorAx)
    MiAxLen = len(intenMajorAx)
    majorAx = np.linspace(-MaAxLen/2, MaAxLen/2, MaAxLen)/measureCoeff
    minorAx = np.linspace(-MiAxLen/2, MiAxLen/2, MiAxLen)/measureCoeff
    plt.plot(majorAx, intenMajorAx, label="Major Axis",)
    plt.plot(minorAx, intenMinorAx, label="Minor Axis",)
    plt.gca().set_ylim(v)
    plt.ylabel(r'$\mu$ (mag)')
    plt.xlabel(r'z')
    plt.legend()
    plt.show()
