#!/usr/bin/env python3
import argparse
from astropy.io import fits
import pathlib as pl
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.colors import LogNorm
import numpy as np
import sky_background as skyb
import photo_slice as pslice
from mpl_events import MplEventDispatcher, mpl
import matplotlib
# import aperture 
matplotlib.use('Qt5Agg')
import scipy.optimize as opt
import pandas as pd

def show(data, v=(900, 18000)):
    yLen, xLen = data.shape
    fig, ax = plt.subplots()
    vmin, vmax = v
    # alternative ways:
    # vmin = max(0.001,np.mean(data) - 3*np.std(data))
    # vmax = max(0.001,np.mean(data) + 3*np.std(data)
    # vmin, vmax = zscale(data)
    im = ax.imshow(data, cmap=plt.cm.bone, origin='lower',
                   norm=LogNorm(vmin=vmin, vmax=vmax),)
    # extent=(-xLen/2,xLen/2,-yLen/2,yLen/2))
    ax.set_title("Show")
    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel('value', rotation=-90, va="bottom")
    plt.show()


def showIsophots(data, v=(22, 16.5), limits=(16.5, 22), count=15):
    fig, ax = plt.subplots()
    vmin, vmax = v
    im = ax.imshow(data, cmap=plt.cm.bone, origin='lower',
                   norm=LogNorm(vmin=vmin, vmax=vmax),)
    ax.set_title("Show Isophots")
    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel('value', rotation=-90, va="bottom")
    ax.contour(data, levels=np.linspace(
        limits[0], limits[1], count), colors='black', alpha=0.5)
    plt.show()


def getMeanIm(imList):
    return sum(imList)/len(imList)


def main_lab1():
    dataDirPath = pl.Path('NGC304_data')

    # Выбор всех файлов bias или flat с помощью масок имён файлов
    biasPathList = dataDirPath.glob('s2050*.fts')
    biasDataList = list(map(lambda x: fits.open(x)[0].data, biasPathList))

    flatPathList = dataDirPath.glob('s2060*.fts')
    flatDataList = list(map(lambda x: fits.open(x)[0].data, flatPathList))

    photoFilter = 'R'
    galPath = dataDirPath / photoFilter / 's2050816.fts'
    galFits = fits.open(galPath)[0]
    galData = galFits.data

    meanBias = getMeanIm(biasDataList)
    meanFlat = getMeanIm(flatDataList)

    # Учёт "нормированного среднего плоского поля"
    normFlat = (meanFlat - meanBias)/np.mean(meanFlat)
    galData = (galData-meanBias)/normFlat
    # Уничтожаем отрицательные значения
    galData[galData < 0.001] = 0.001

    ''' 
    Интерполяция фона:

    Будет открыто окно, где последовательно sample раз нужно
    будет выбрать регионы для интерполяции по ним фона.

    Выбирать нужно пояледовательно кликая ЛКМ, отмечая
    таким образом диагональные углы прямоугольной области.

    '''
    # skyBack = skyb.getSkyBackground(galData, sample=10, k=(1, 1))
    # Показать полученный фон неба

    # Сохрание полученного фона неба в файл (в имени расширение не писать)
    # skyb.saveSky(skyBack, name="skyBack" + photoFilter)

    # Загрузка фона неба из файла (в имени расширение не писать)
    skyBack = skyb.loadSky(name="skyBack" + photoFilter)
    # plt.matshow(skyBack)

    galData = galData - skyBack
    show(galData)

    # Время экспозиции, масштаб и зенитное расстояние
    t = float(galFits.header['EXPTIME'])
    S = float(galFits.header['IMSCALE'].split()[0])
    z = float(galFits.header['Z'])

    # поправочные расстояния для разных фильтров
    zeroPoint = {
        "B": 26.5,
        "V": 26.3,
        "R": 26.4,
        "I": 26.0,
    }
    K = {
        "B": 0.34,
        "V": 0.21,
        "R": 0.15,
        "I": 0.08,
    }

    # Перевод в станд. зв. велечины и учёт поправок атмосферы
    galData = -2.5*np.log10(galData/t) + 2.5*np.log10(S)
    galData = galData - K[photoFilter]/np.cos(z*np.pi/180)
    galData = galData + zeroPoint[photoFilter]
    # заменяем nan значения на некоторый минимум
    galData = np.nan_to_num(galData, copy=False, nan=zeroPoint[photoFilter],
                            posinf=None, neginf=None)

    # диапозон значений для отображения
    v = (25, 17)
    # просто показывает изофоты
    showIsophots(galData, v=v, limits=(v[1], v[0]), count=10)

    '''
    Нахождение центра и угла большой оси:

    Будет открыто окно, где последовательно нужно отметить ЛКМ
    приблизительный центр галактики, после чего он автоматически
    определится по максимуму в небольшой области и отобразится.

    Далее нужно отметить ЛКМ, ориентируясь на частые изофоты, любое
    направление большой оси.

    ang - угол между большой осью и OX
    '''
    xCen, yCen, ang = pslice.selectCenterAndAng(galData, v=v)
    print(xCen, yCen, ang)

    # Строит центрированный график среза по большой и малой оси
    pslice.getSlice(galData, xCen, yCen, ang=ang,
                    galBoxSize=200, v=v, measureCoeff=4)

def fluxToMag(flux, t, zeroPoint):
    mag = -2.5*np.log10(flux/t) + zeroPoint
    return mag 

def getStarMag(data, coords, r, rWings, rBack):
    x, y = coords
    yLen, xLen = data.shape
    yRing = np.arange(y-round(r), y+round(r))
    xRing = np.arange(x-round(r), x+round(r))
    yRing = yRing[(yRing >= 0) & (yRing < yLen)]
    xRing = xRing[(xRing >= 0) & (xRing < xLen)]
    res = 0
    # plt.imshow(data)
    for yi in yRing:
        phi = np.arcsin((yi - y)/r)
        xByY = xRing[xRing < x+r*np.cos(phi)]
        xByY = xByY[xByY > x-r*np.cos(phi)]
        # plt.scatter([xByY], [yi for i in range(len(xByY))],marker=',',color='red')
        res += sum(data[yi][xByY])
    # plt.show()
    return res


class SelectStarsDispatcher(MplEventDispatcher):
    def __init__(self, figure, count=1):
        super().__init__(figure)
        self.fig = figure
        self.count = count
        self.coordsList = []

    def on_mouse_button_release(self, event: mpl.MouseEvent):
        coords = (int(event.xdata), int(event.ydata))
        self.coordsList.append(coords)
        if len(self.coordsList) == self.count:
            plt.close('all')
        else:
            self.fig.axes[0].scatter(coords[0], coords[1])
            self.fig.canvas.draw_idle()

def gaussian2D(coords, amplitude, x0, y0, sigmaX, sigmaY):
    x, y = coords
    res = amplitude * np.exp(-((x-x0)**2/sigmaX**2 + (y-y0)**2/sigmaY**2)/2)
    return res.ravel()

def getFWHM(data, coords, width):
    x, y = coords
    region = data[y-width:y+width, x-width:x+width]
    yLen, xLen = region.shape
    xGauss = np.linspace(0, xLen, xLen)
    yGauss = np.linspace(0, yLen, yLen)
    xGauss, yGauss = np.meshgrid(xGauss, yGauss)
    initialGuess = (5e3, width, width, 1, 1)
    # if sigma < 0:
    # print(popt)
    # plt.matshow(gaussian2D((xGauss, yGauss), *popt).reshape(xLen,yLen))
    # plt.matshow(region)
    # plt.matshow(data)
    # plt.show()
    popt, pcov = opt.curve_fit(gaussian2D, (xGauss,yGauss), region.ravel(), p0=initialGuess)
    sigma = (abs(popt[3]) + abs(popt[4]))/2
    fwhm = 2.35482*sigma
    cen = (int(round(popt[1] + x - width)), int(round(popt[2] + y - width)))
    return fwhm, cen

def getStarMagSeries(HDUList, coordsList, fwhmSeries, zeroPoint=[]):
    magList = []
    if not len(zeroPoint):
        zeroPoint = [0 for i in range(len(coordsList))]
    for u, c, f, z in zip(HDUList, coordsList, fwhmSeries, zeroPoint):
        flux = getStarMag(u.data, c, f*3+1, 2, 2)
        magList.append(fluxToMag(flux, u.header['EXPTIME'], z))
    return np.array(magList)

def getZeroPoint(rawStd1, rawStd2, photoFilter):
    charOfStd = {
    'j': (8.71, 10.31),
    'h': (7.80, 8.01),
    'k': (7.42, 6.93)
    }
    delta1 = charOfStd[photoFilter][0] - rawStd1
    delta2 = charOfStd[photoFilter][1] - rawStd2
    zeroPoint = (delta1 + delta2)/2
    return zeroPoint

def getFWHMSeries(HDUList, coordsList, width=10):
    newCoordsList = []
    fwhmList = []
    for u, c in zip(HDUList, coordsList):
        fwhm, cen = getFWHM(u.data, c, width)
        newCoordsList.append(cen)
        fwhmList.append(fwhm)
    return np.array(fwhmList), newCoordsList


def main_lab2():
    dataDirPath = pl.Path('StarPhot')
    photoFilter = 'j'
    jDirPath = dataDirPath / photoFilter
    jPathList = jDirPath.glob(photoFilter + '*.fit')
    jHDUList = list(map(lambda x: fits.open(x)[0], jPathList))
    jHDUList = sorted(jHDUList, key=lambda x: float(x.header['MJD_OBS']))

    # # Тыкаем на звёзды в указанном порядке
    # with open(photoFilter + '_coords.csv', 'a') as coordsFile:
    #     coordsFile.write('MJD,star_x,star_y,standart_1_x,standart_1_y,standart_2_x,standart_2_y\n')
    # for u in jHDUList:
    #     fig, ax = plt.subplots()
    #     disp = SelectStarsDispatcher(fig, 3)
    #     ax.imshow(u.data, vmin=0,vmax=10000)
    #     ax.invert_yaxis()
    #     mng = plt.get_current_fig_manager()
    #     mng.window.showMaximized()
    #     plt.show()
    #     with open(photoFilter + '_coords.csv', 'a') as coordsFile:
    #         line = str(u.header['MJD_OBS']) + ',' + ','.join(list(map(lambda x: str(x[0])+','+str(x[1]), disp.coordsList))) + '\n'
    #         coordsFile.write(line)

    # Определяем зв. величины
    dfCoords = pd.read_csv(photoFilter + '_coords.csv', index_col=0, sep=',')

    # coordsListStar = [i for i in zip(dfCoords.star_x, dfCoords.star_y)]
    # coordsListStd1 = [i for i in zip(dfCoords.standart_1_x, dfCoords.standart_1_y)]
    # coordsListStd2 = [i for i in zip(dfCoords.standart_2_x, dfCoords.standart_2_y)]

    # fwhmStd1, coordsListStd1 = getFWHMSeries(jHDUList, coordsListStd1, 10)
    # fwhmStd2, coordsListStd2 = getFWHMSeries(jHDUList, coordsListStd2, 10)
    # fwhmStar, coordsListStar = getFWHMSeries(jHDUList, coordsListStar, 10)
    # fwhmSeries =  (fwhmStd1 + fwhmStd2 + fwhmStar)/3

    # rawMagStd1 = getStarMagSeries(jHDUList, coordsListStd1, fwhmSeries)
    # rawMagStd2 = getStarMagSeries(jHDUList, coordsListStd2, fwhmSeries)
    # zeroPoint = getZeroPoint(rawMagStd1, rawMagStd2, photoFilter)
    # magStd1 = rawMagStd1 + zeroPoint
    # magStd2 = rawMagStd2 + zeroPoint
    # magStar = getStarMagSeries(jHDUList, coordsListStar, fwhmSeries, zeroPoint)

    # # Сохраняем полученные зв. величины
    # dfMags = pd.DataFrame(columns=['MJD','star','std1','std2'])
    # dfMags['MJD'] = dfCoords.index
    # dfMags['star'] = magStar
    # dfMags['std1'] = magStd1
    # dfMags['std2'] = magStd2
    # dfMags.to_csv('mags_' + photoFilter + '.csv', sep=',')

    # Подгружаем полученные зв. величины
    # dfMags = pd.read_csv('mags_' + photoFilter + '.csv', index_col=0, sep=',')
    # magStar = dfMags['star']
    # magStd1 = dfMags['std1']
    # magStd2 = dfMags['std2']

    # Графики кривой блеска по дням и по фазе
    # plt.figure(figsize = (15,5))
    # plt.plot(dfCoords.index, magStar, dfCoords.index, magStd1, dfCoords.index, magStd2)
    # plt.scatter(dfCoords.index, magStar, marker='.', color='darkblue')
    # plt.scatter(dfCoords.index, magStd1, marker='.', color='chocolate')
    # plt.scatter(dfCoords.index, magStd2, marker='.', color='darkgreen')
    # plt.gca().invert_yaxis()
    # plt.savefig(photoFilter+'.pdf')
    # plt.show()

    # period = 590
    # plt.figure(figsize = (10,5))
    # plt.scatter((dfCoords.index%period)/period, magStar, color='red')
    # plt.savefig(photoFilter+'_phaz.pdf')
    # plt.show()

    # Графики показателя цвета
    dfMagsJ = pd.read_csv('mags_j.csv', index_col=0, sep=',')
    jdJ = dfMagsJ['MJD']
    magStarJ = dfMagsJ['star']
    magStd1J = dfMagsJ['std1']
    magStd2J = dfMagsJ['std2']

    dfMagsH = pd.read_csv('mags_h.csv', index_col=0, sep=',')
    jdH = dfMagsH['MJD']
    magStarH = dfMagsH['star']
    magStd1H = dfMagsH['std1']
    magStd2H = dfMagsH['std2']

    dfMagsK = pd.read_csv('mags_k.csv', index_col=0, sep=',')
    jdK = dfMagsK['MJD']
    magStarK = dfMagsK['star']
    magStd1K = dfMagsK['std1']
    magStd2K = dfMagsK['std2']



    dfMagsK['MJD'] = dfMagsK['MJD'].apply(lambda x: round(x,2))
    dfMagsJ['MJD'] = dfMagsJ['MJD'].apply(lambda x: round(x,2))
    dfMagsH['MJD'] = dfMagsH['MJD'].apply(lambda x: round(x,2))
    j_k = (-(dfMagsK[dfMagsK['MJD'].isin(dfMagsJ['MJD'])]['star']).values + (dfMagsJ[dfMagsJ['MJD'].isin(dfMagsK['MJD'])]['star']).values)
    h_k = (-(dfMagsK[dfMagsK['MJD'].isin(dfMagsH['MJD'])]['star']).values + (dfMagsH[dfMagsH['MJD'].isin(dfMagsK['MJD'])]['star']).values)
    plt.figure(figsize = (15,5))
    plt.plot(dfMagsK[dfMagsK['MJD'].isin(dfMagsJ['MJD'])]['MJD'], j_k)
    # plt.plot(dfMagsK[dfMagsK['MJD'].isin(dfMagsH['MJD'])]['MJD'], h_k)
    plt.scatter(dfMagsK[dfMagsK['MJD'].isin(dfMagsJ['MJD'])]['MJD'], j_k, marker='.', color='darkblue')
    # plt.scatter(dfMagsK[dfMagsK['MJD'].isin(dfMagsH['MJD'])]['MJD'], h_k, marker='.', color='darkblue')
    plt.gca().invert_yaxis()
    plt.savefig('j_k.pdf')
    plt.show()



if __name__ == "__main__":
    main_lab2()
